﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BE_clientes;
using BE_clientes.Models;

namespace BE_clientes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TarjetaClientesController : ControllerBase
    {
        private readonly AplicationDbContext _context;

        public TarjetaClientesController(AplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/TarjetaClientes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TarjetaCliente>>> GetTarjetaClientes()
        {
            return await _context.TarjetaClientes.ToListAsync();
        }

        // GET: api/TarjetaClientes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TarjetaCliente>> GetTarjetaCliente(int id)
        {
            var tarjetaCliente = await _context.TarjetaClientes.FindAsync(id);

            if (tarjetaCliente == null)
            {
                return NotFound();
            }

            return tarjetaCliente;
        }

        // PUT: api/TarjetaClientes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTarjetaCliente(int id, TarjetaCliente tarjetaCliente)
        {
            if (id != tarjetaCliente.Id)
            {
                return BadRequest();
            }

            _context.Entry(tarjetaCliente).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TarjetaClienteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TarjetaClientes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TarjetaCliente>> PostTarjetaCliente(TarjetaCliente tarjetaCliente)
        {
            _context.TarjetaClientes.Add(tarjetaCliente);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTarjetaCliente", new { id = tarjetaCliente.Id }, tarjetaCliente);
        }

        // DELETE: api/TarjetaClientes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TarjetaCliente>> DeleteTarjetaCliente(int id)
        {
            var tarjetaCliente = await _context.TarjetaClientes.FindAsync(id);
            if (tarjetaCliente == null)
            {
                return NotFound();
            }

            _context.TarjetaClientes.Remove(tarjetaCliente);
            await _context.SaveChangesAsync();

            return tarjetaCliente;
        }

        private bool TarjetaClienteExists(int id)
        {
            return _context.TarjetaClientes.Any(e => e.Id == id);
        }
    }
}
