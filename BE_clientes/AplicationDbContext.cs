﻿using BE_clientes.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BE_clientes
{
    public class AplicationDbContext: DbContext
    {
        //constuctor
        public AplicationDbContext(DbContextOptions<AplicationDbContext> options): base(options)
        {

        }

        //propiedad
        public DbSet<TarjetaCliente> TarjetaClientes { get; set; }
    }
}
