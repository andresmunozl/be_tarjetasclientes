﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BE_clientes.Models
{
    public class TarjetaCliente
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "character varying(100)")]
        public string Titular { get; set; }

        [Required]
        [Column(TypeName = "character varying(16)")]
        public string NumeroTarjeta { get; set; }

        [Required]
        [Column(TypeName = "character varying(5)")]
        public string FechaExpiracion { get; set; }

        [Required]
        [Column(TypeName = "character varying(3)")]
        public string CVV { get; set; }
    }
}
